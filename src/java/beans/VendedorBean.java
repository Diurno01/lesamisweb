/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;

/**
 *
 * @author Administrador
 */

    public class VendedorBean  extends UsuarioBean implements Serializable{
        
        private int numUs;
        private String nombre;
        private String apellido;
        private String sucursal;

    public int getNumUs() {
        return numUs;
    }

    public void setNumUs(int numUs) {
        this.numUs = numUs;
    }


    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }


    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

   
}
