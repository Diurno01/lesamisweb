/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;

/**
 *
 * @author Administrador
 */
public class VueloBean implements Serializable {
    
    private int numero;
    private String fecha;
    private String hora;
    private String origen;
    private String destinto;
    private int plazasTotales;
    private int plazasTurDisponibles;
    private int plazasPriDisponibles;

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestinto() {
        return destinto;
    }

    public void setDestinto(String destinto) {
        this.destinto = destinto;
    }

    public int getPlazasTotales() {
        return plazasTotales;
    }

    public void setPlazasTotales(int plazasTotales) {
        this.plazasTotales = plazasTotales;
    }

    public int getPlazasTurDisponibles() {
        return plazasTurDisponibles;
    }

    public void setPlazasTurDisponibles(int plazasTurDisponibles) {
        this.plazasTurDisponibles = plazasTurDisponibles;
    }

    public int getPlazasPriDisponibles() {
        return plazasPriDisponibles;
    }

    public void setPlazasPriDisponibles(int plazasPriDisponibles) {
        this.plazasPriDisponibles = plazasPriDisponibles;
    }
    
}
