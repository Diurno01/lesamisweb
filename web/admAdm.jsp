<%-- 
    Document   : admAdm
    Created on : 09-oct-2018, 18:05:19
    Author     : Administrador
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" type="image/x-icon" href="/images/icono.ico" />
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <title>Administrar Admins</title>
    </head>
    <body>
        <h1>Administrar Admins</h1>
        <p>
            <a href="agregarAdm.jsp" style="text-decoration:none"><input type="button" value="Agregar Admin" class="boton"></a>
            <br><br>
            <a href="BorrarUser?type=ADMIN" style="text-decoration:none"><input type="button" value="Borrar Admin" class="boton"></a>
            <br><br>
            <a href="" style="text-decoration:none"><input type="button" value="Modificar Admin" class="boton"></a>
            <br><br>
            <a href="admUs.jsp" style="text-decoration:none"><input type="button" value="Volver" class="boton"></a>
        </p>
                   
    </body>

