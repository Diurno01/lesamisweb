<%-- 
    Document   : admMenu
    Created on : 08-oct-2018, 21:19:35
    Author     : Administrador
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" type="image/x-icon" href="/images/icono.ico" />
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <title>Menu de admin</title>
    </head>
    <body>
        <h1>Menú Admin</h1>
        <p>
            <a href="admUs.jsp" style="text-decoration:none"><input type="button" value="Administrar usuarios" class="boton"></a>
            <br><br>
            <a href="" style="text-decoration:none"><input type="button" value="Administrar reservas" class="boton"></a>
            <br><br>
            <a href="" style="text-decoration:none"><input type="button" value="Administrar vuelos" class="boton"></a>
            <br><br>
            <a href="" style="text-decoration:none"><input type="button" value="Administrar hospedajes" class="boton"></a>
            <br><br>
            <a href="" style="text-decoration:none"><input type="button" value="Administrar sucursales" class="boton"></a>
            <br><br>
            <a href="index.jsp" style="text-decoration:none"><input type="button" value="Salir" class="boton"></a>
        </p>
    </body>
</html>
