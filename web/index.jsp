<%-- 
    Document   : index
    Created on : 02-oct-2018, 21:12:49
    Author     : Administrador
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <title>Les Amis</title>
        <link rel="shortcut icon" type="image/x-icon" href="/images/icono.ico" />
    </head>
    <body>
        <h1>Bienvenido al sistema de Les Amis</h1>
        <h2>Login</h2>

        <form method="post" action="Login">
             <p>
                Usuario:                      
                <input type="text" name="nombre">
                <br>Password: 
                <input type="password" name="password" >
                <br><br><br>
                <input type ="submit" value="Entrar" class="boton">
            </p>
        </form>
    </body>
</html>
