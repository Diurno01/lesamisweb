/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;

/**
 *
 * @author Administrador
 */
public class SucursalBean implements Serializable{
        private String codigo;
        private String direccion;
        private long telefono;

    
        
        public long getTelefono() {
        return telefono;
    }

    public void setTelefono(long telefono) {
        this.telefono = telefono;
    }

    
    public String getCodigo() {
        return codigo;
    }

    public String getDireccion() {
        return direccion;
    }

    

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

   
}