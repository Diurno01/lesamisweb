/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;

/**
 *
 * @author Administrador
 */
public class ClienteBean extends UsuarioBean implements Serializable {
  
    private int numUs;
    private String codigo;
    private String nombre;
    private String apellido;
    private String direccion;
    private long telefono; 
    private String sucursalContratada;

    public int getNumUs() {
        return numUs;
    }

    public void setNumUs(int numUs) {
        this.numUs = numUs;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public long getTelefono() {
        return telefono;
    }

    public void setTelefono(long telefono) {
        this.telefono = telefono;
    }

    public String getSucursalContratada() {
        return sucursalContratada;
    }

    public void setSucursalContratada(String sucursalContratada) {
        this.sucursalContratada = sucursalContratada;
    }
    
}
