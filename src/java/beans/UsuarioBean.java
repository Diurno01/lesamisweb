/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;

/**
 *
 * @author Administrador
 */
public class UsuarioBean  implements Serializable{
    
    private int numUs;
    private String id;
    private String password;
    private String uTipo;

   
    public static enum Tipo{
        ADMIN,VENDEDOR,CLIENTE
    }

    public int getNumUs() {
        return numUs;
    }

    public void setNumUs(int numUs) {
        this.numUs = numUs;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getuTipo() {
        return uTipo;
    }

    public void setuTipo(String uTipo) {
        this.uTipo = uTipo;
    }

    
}
