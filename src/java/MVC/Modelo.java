/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

import beans.ClienteBean;
import beans.UsuarioBean;
import beans.VendedorBean;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class Modelo {
    private String jdbcDriver;
    private String dbName;
    private String urlRoot;
    private int numUser;
    private ArrayList<UsuarioBean> userList;
    private UsuarioBean resultado;
    private ActionListener listener;
    

    public Modelo(String url, String dbName) {
        jdbcDriver = "com.mysql.jdbc.Driver";
        urlRoot = "jdbc:mysql://" + url + "/";
        this.dbName = dbName;
        userList =  new ArrayList();
        listener = null;
        resultado = null;
        try {
            Class.forName(jdbcDriver);
        } catch (ClassNotFoundException e) {
            reportException(e.getMessage());
        }
    }
    
    public void consultarUsuario(String nombre, String pass) {
        try {
            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            
            stmt.execute("SELECT tipo FROM usuarios WHERE ID='" 
                                                    + nombre + "' AND pass='"+ pass +"'");
            
            ResultSet rs = stmt.getResultSet();
            if (rs.next()) {
                UsuarioBean usuario = new UsuarioBean();
                usuario.setId(nombre);
                usuario.setPassword(pass);
                //usuario.setNumUs(rs.getInt(1));
                usuario.setuTipo(rs.getString("tipo"));
                
                resultado = usuario;
                
            }
            con.close();
        } catch (SQLException e) {
            reportException(e.getMessage());
        }

    }
  
    public void agregarAdm(String nombre, String pass) {
        try {
            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
           
            stmt.execute("SELECT * FROM `usuarios` WHERE `numUsuario`");
            ResultSet rs = stmt.getResultSet();
            rs.last();
            numUser = rs.getInt("numUsuario");
            
            stmt.execute("INSERT INTO `usuarios` (`numUsuario`, `ID`, `pass`, `tipo`) VALUES ('"+(numUser+1)+"', '"+nombre+"', '"+pass+"', 'ADMIN')");
            
            con.close();
        } catch (SQLException e) {
            reportException(e.getMessage());
        }

    }
    
    public void listarUsuarios( String tipo) {
        try {
            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
           
            stmt.execute("SELECT * FROM `usuarios` WHERE `tipo`= '"+tipo+"'");
            ResultSet rs = stmt.getResultSet();
          
            switch(tipo){
                    case "ADMIN":
                          while (rs.next()){
                            UsuarioBean usuario = new UsuarioBean();
                             
                             usuario.setNumUs(rs.getInt("numUsuario"));
                             usuario.setId(rs.getString("ID"));
                             usuario.setPassword(rs.getString("pass"));
                             usuario.setuTipo("ADMIN");
                             userList.add(usuario);
                          }
                             
                        break;
                    case "VENDEDOR":
                        while (rs.next()){
                            VendedorBean usuario = new VendedorBean();
                             usuario.setNumUs(rs.getInt("numUsuario"));
                             usuario.setId(rs.getString("ID"));
                             usuario.setPassword(rs.getString("pass"));
                             usuario.setuTipo("VENDEDOR");
                             
                             
                             userList.add(usuario);
                
                         }
                        break;
                    case "CLIENTE":
                        while (rs.next()){
                            ClienteBean usuario = new ClienteBean();
                             usuario.setNumUs(rs.getInt("numUsuario"));
                             usuario.setId(rs.getString("ID"));
                             usuario.setPassword(rs.getString("pass"));
                             usuario.setuTipo("CLIENTE");
                             
                             userList.add(usuario);
                
                         }
                        break;
                }
           
           
            
           
            
            con.close();
        } catch (SQLException e) {
            reportException(e.getMessage());
        }
        
        
    }

    public ArrayList<UsuarioBean> getUserList() {
        return userList;
    }
    
    
    public UsuarioBean getResultado() {
        return resultado;
    }

    private void reportException(String exception) {
        if (listener != null) {
            ActionEvent evt = new ActionEvent(this, 0, exception);
            listener.actionPerformed(evt);
        }
    }

    public void addExceptionListener(ActionListener listener) {
        this.listener = listener;
    }
}
