/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrador
 */
@WebServlet(name = "AgregarAdm", urlPatterns = {"/AgregarAdm"})
public class AgregarAdm extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    HttpServletRequest request;
    HttpServletResponse response;
    
    
    @Override
     protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        this.request = request;
        this.response = response;
        String ip = "localhost";
        String bd = "lesamis";
        
        String nombre = request.getParameter("nombre");
        String pass = request.getParameter("password");
        
        Modelo m = new Modelo(ip, bd);
     
        m.addExceptionListener(new AgregarAdm.ExceptionListener() {});
        //hacer funcion de agregar usuario en modelo
        m.agregarAdm(nombre, pass);
        
        String paginaSiguiente = "userAgregado.jsp";
        request.setAttribute("tipo","ADMIN");
        request.setAttribute("jsp", "admMenu.jsp");
        
        RequestDispatcher vista = request.getRequestDispatcher(paginaSiguiente);
        
        vista.forward(request, response);
        
    }

    
    
    
    
    private class ExceptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String exception = event.getActionCommand();
            request.setAttribute("mensajeError", exception);
            RequestDispatcher vista = request.getRequestDispatcher("error.jsp");
            try {
                vista.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
  

}
