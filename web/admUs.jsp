<%-- 
    Document   : admUs
    Created on : 09-oct-2018, 17:52:20
    Author     : Administrador
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" type="image/x-icon" href="/images/icono.ico" />
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <title>Administrar Usuarios</title>
    </head>
    <body>
        <h1>Administrar Usuarios</h1>
        <p>
            <a href="admAdm.jsp" style="text-decoration:none"><input type="button" value="Administrar Admins" class="boton"></a>
            <br><br>
            <a href="" style="text-decoration:none"><input type="button" value="Administrar Vendedores" class="boton"></a>
            <br><br>
            <a href="" style="text-decoration:none"><input type="button" value="Administrar Clientes" class="boton"></a>
            <br><br>
            <a href="admMenu.jsp" style="text-decoration:none"><input type="button" value="Volver" class="boton"></a>
        </p>
    </body>
</html>
