<%-- 
    Document   : userAgregado
    Created on : 15-oct-2018, 0:56:04
    Author     : Administrador
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="shortcut icon" type="image/x-icon" href="/images/icono.ico" />
        <title>Usuario agregado</title>
    </head>
    <body>
        <h1>Has agregado un ${tipo} con exito</h1>
        <br><br>
        
        <p>
             <a href="${jsp}" style="text-decoration:none"><input type="button" value="Volver" class= "boton" ></a>
        </p>
  
    </body>
</html>
