<%-- 
    Document   : borrarUser
    Created on : 15-oct-2018, 17:30:57
    Author     : Administrador
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="shortcut icon" type="image/x-icon" href="/images/icono.ico" />
        <title>Borrar usuario</title>
    </head>
    <body>
    <c:choose>  
       <c:when test= "${empty usuarios}" >
        <p>
            <h1> Error no hay usuarios a ${action}</h1>
        </p>
        
       </c:when>
       <c:otherwise>
           <form method="post" action="">
                        <h1>Elija el numero usuario que desea ${action}</h1>
                           <c:forEach var="UsuarioBean" items="${usuarios}">
                                   <p>
                                       Numero de Usuario: "${UsuarioBean.numUs}" &nbsp; ID de usuario: "${UsuarioBean.id}" <br><br>
                                   </p>

                           </c:forEach> 
                        
                        
                        
                        <p>
                            
                            <input type="text"  name="datoIngresado">
                            <br><br>
                            <input type ="submit" value="Aceptar" class="boton">
                            <br><br>
                            <a href="${jsp}" style="text-decoration:none"><input type="button" value="Volver" class="boton"></a>
                        </p>
           </form>
            
       </c:otherwise>
      </c:choose> 
    </body>
</html>

