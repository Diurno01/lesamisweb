/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

import beans.UsuarioBean;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;






@WebServlet(name = "BorrarUser", urlPatterns = {"/BorrarUser"})
public class BorrarUser extends HttpServlet {

    
    
    HttpServletRequest request;
    HttpServletResponse response;
    
    
    
    
   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
        
        this.request = request;
        this.response = response;
        String ip = "localhost";
        String bd = "lesamis";
        String tipo = request.getParameter("type");
        
        Modelo m = new Modelo(ip, bd);
     
        m.addExceptionListener(new BorrarUser.ExceptionListener() {});
        
        
       
        
        
        
        m.listarUsuarios(tipo);
        
        ArrayList<UsuarioBean> lista = m.getUserList();
        
        String paginaSiguiente = "buscarUser.jsp";
        request.setAttribute("usuarios",lista);
        request.setAttribute("jsp", "admMenu.jsp");
        request.setAttribute("action", "eliminar");
        
        RequestDispatcher vista = request.getRequestDispatcher(paginaSiguiente);
        
        vista.forward(request, response);
        
        
    }

   private class ExceptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String exception = event.getActionCommand();
            request.setAttribute("mensajeError", exception);
            RequestDispatcher vista = request.getRequestDispatcher("error.jsp");
            try {
                vista.forward(request, response);
            } catch (ServletException | IOException ex) {
                ex.printStackTrace();
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
